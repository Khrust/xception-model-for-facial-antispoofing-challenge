import json
from keras.models import model_from_json
import numpy as np
from xceptionmodel import XceptionModel
from pathlib import Path
from preprocessing import get_augmented_crops
import argparse
import cv2
import pandas as pd
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='This program runs trained neural network.')
    parser.add_argument('test_dir', type=str, help='Absolute path to directory of test data.')
    parser.add_argument('json_model', type=str, help='Absolute path to json model.')
    parser.add_argument('weights', type=str, help='Absolute path to model weights of trained model.')
    parser.add_argument('img_width', type=int, help='Input image width.')
    parser.add_argument('img_height', type=int, help='Input image height.')
    parser.add_argument('submission', type=str, help='Absolute path to output file (csv).')
    args = parser.parse_args()

    # load model
    model = model_from_json(json.load(Path(args.json_model).open(mode='r')))

    # load weights
    model.load_weights(str(Path(args.weights)))

    xception_model = XceptionModel(nb_classes=2, frame_width=args.img_width, frame_height=args.img_height, batch_size=1)
    xception_model.model = model

    submission = pd.DataFrame({'0': [], '2': []}).astype(np.float32)
    img_counter = 1

    test_data = list(Path(args.test_dir).glob('*.png'))
    test_data_size = len(test_data)

    # test time augmentation
    for img_path in test_data:
        test_img = cv2.imread(str(img_path))
        if test_img.shape[0] < args.img_height or test_img.shape[1] < args.img_width:
            test_img = cv2.resize(test_img, (args.img_width, args.img_height))

        crop_predictions = xception_model.predict(
            np.array(get_augmented_crops(test_img, args.img_width, args.img_height)))
        prediction = np.array([np.sum(crop_predictions[:, 0]), np.sum(crop_predictions[:, 1])]) / len(crop_predictions)
        submission = submission.append({'0': img_path.name, '2': prediction[0]}, ignore_index=1)

        if img_counter % 200 == 0:
            print("%d of %d images processed" % (img_counter, test_data_size))
        img_counter += 1
    submission.to_csv(args.submission, index=0)