from xceptionmodel import XceptionModel
from pathlib import Path
import json
from sklearn.model_selection import train_test_split
from keras.utils import to_categorical
import numpy as np

if __name__ == "__main__":

    # get list of all files of train folder
    crops_dir = Path.cwd().joinpath('crops')
    crops_real = crops_dir.joinpath('real')
    crops_spoof = crops_dir.joinpath('spoof')

    train_data = []

    for p in list(crops_real.glob('*.png')):
        train_data.append(p)
    for p in list(crops_spoof.glob('*.png')):
        train_data.append(p)

    one_hot_labels = []
    for img_path in train_data:
        label = img_path.parts[-2]
        if label == 'real':
            label = 0
        elif label == 'spoof':
            label = 1

        one_hot_labels.append(to_categorical(label, 2))

    x_train, x_val, y_train, y_val = train_test_split([str(path_item) for path_item in train_data],
                                                      np.array(one_hot_labels),
                                                      test_size=0.2,
                                                      random_state=42)

    model_name = 'xception_model'
    model = XceptionModel(nb_classes=2,
                          frame_width=300,
                          frame_height=300,
                          batch_size=8,
                          weights_folder=str(Path.cwd().joinpath(model_name)),
                          nb_epoch=25,
                          save_best_only=True,
                          save_weights_only=True,
                          model_name=model_name)

    model.build()

    # save model architecture
    Path.cwd().joinpath('xception_model_300_300_3.json').open(mode='w').write(json.dumps(model.model.to_json()))

    # loading data in ram
    model.load_images_in_ram([str(img_path) for img_path in train_data])

    # train model
    model.train(x_train, y_train, x_val, y_val)