<h2>Xception model for facial antispoofing challenge</h2>

This is a baseline solution for ID R&D facial antispoofing challenge.
<br>See information about competition on https://boosters.pro/championship/idrnd/overview

<b>Note</b> that before run preprocessing.py, you must: 
1. Move all images of subdirectories <i>IDRND_FASDB_train/real</i>, <i>IDRND_FASDB_train/spoof</i>, <i>IDRND_FASDB_val/real</i>,
<i>IDRND_FASDB_val/spoof</i> to the <i>train/real</i> and <i>train/spoof</i> folders.
2. Remove all images which names starts with <b>._</b> (dot and underscore).


<h3>File launch sequence</h3>

1. <b>preprocessing.py </b> - cropping photos and augmenting they by flipping horizontally and vertically their copies.
2. <b>train.py</b> - train xception model.
3. <b>runtrainedmodel.py</b> - predict unlabeled data.

Template of launching <b>runtrainedmodel.py</b>:

`runtrainedmodel.py <path_to_test_data> <path_to_json_model> <path_to_weights> <width> <height> <path_to_submission>`

<b>Example:</b> <br>`runtrainedmodel.py /home/username/idrnd/test/ /home/username/idrnd/xception_model_300_300_3.json /home/username/idrnd/xception_model/xception_model_300_300_05_0.9950.h5 300 300 /home/username/idrnd/submission.csv`

